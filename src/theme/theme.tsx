export const theme = {
  colors: {
    primary: '#ab47bc',
    light: '#df78ef',
    dark: '#790e8b',
  },
};
