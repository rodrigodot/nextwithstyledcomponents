import { ThemeProvider } from 'styled-components';
import { AppProps } from 'next/app';

import { GlobalStyle, theme } from '../theme';

export default function App({ Component, pageProps }: AppProps) {
  return (
    <>
      <GlobalStyle />
      <ThemeProvider theme={theme}>
        <Component {...pageProps} />
      </ThemeProvider>
    </>
  );
}
